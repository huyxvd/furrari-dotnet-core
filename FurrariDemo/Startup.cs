using FurrariDemo.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            #region DI
            services.AddTransient<IFurrari, Furrari>();


            services.AddSingleton<ICountService, CountService>();

            services.AddTransient<IFirstCountService, FirstCountService>();
            services.AddTransient<ISecondCountService, SecondCountService>();
            // Register the Swagger services
            services.AddSwaggerDocument();
            #endregion

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            #region middleware demo
            //app.Use(async (context, next) =>
            //{
            //   Console.WriteLine("middle ware 1 before");
            //   await next.Invoke();
            //   Console.WriteLine("middle ware 1 after");
            //});

            //app.Use(async (context, next) =>
            //{
            //    Console.WriteLine("middle ware 2 before");
            //    await next.Invoke();
            //    Console.WriteLine("middle ware 2 after");
            //});

            //app.Run(async context =>
            //{
            //    await context.Response.WriteAsync("Hello, World!");
            //});
            #endregion

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();


            }

            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                #region Route
                endpoints.MapGet("/route", async context =>
                {
                    await context.Response.WriteAsync("Hello World!");
                });
                #endregion
            });

            #region swagger
            //Install-Package NSwag.AspNetCore
            // Register the Swagger generator and the Swagger UI middlewares
            app.UseOpenApi();
            app.UseSwaggerUi3();
            #endregion
        }
    }
}
