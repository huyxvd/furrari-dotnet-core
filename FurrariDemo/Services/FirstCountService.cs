﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FurrariDemo.Services
{
    public interface IFirstCountService
    {
        int GetCounter();
    }

    public class FirstCountService : IFirstCountService
    {
        private ICountService _count;

        public FirstCountService(ICountService count)
        {
            _count = count;
        }

        public int GetCounter()
        {
            return _count.GetCounter();
        }
    }
}
