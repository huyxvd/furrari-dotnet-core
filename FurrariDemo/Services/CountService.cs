﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FurrariDemo.Services
{
    public interface ICountService
    {
        int GetCounter();
    }

    public class CountService : ICountService
    {
        private int _count;

        public CountService()
        {
            _count = 0;
        }

        public int GetCounter()
        {
            return ++_count;
        }
    }
}
