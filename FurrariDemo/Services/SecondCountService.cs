﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FurrariDemo.Services
{
    public interface ISecondCountService
    {
        int GetCounter();
    }

    public class SecondCountService : ISecondCountService
    {
        private ICountService _count;

        public SecondCountService(ICountService count)
        {
            _count = count;
        }

        public int GetCounter()
        {
            return _count.GetCounter();
        }
    }
}
