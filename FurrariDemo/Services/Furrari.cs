﻿using System;

namespace FurrariDemo.Services
{
    public interface IFurrari
    {
        string GoodJob();
    }

    public class Furrari : IFurrari
    {
        public string GoodJob()
        {
            return "GoodJob";
        }
    }

    //public class FurrariV2 : IFurrari
    //{
    //    public string GoodJob()
    //    {
    //        return "GoodJob $$$";
    //    }
    //}
}
