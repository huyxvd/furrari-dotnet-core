﻿using FurrariDemo.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FurrariDemo.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class LifetimeController : ControllerBase
    {
        private readonly IFirstCountService firstCount;
        private readonly ISecondCountService secondCount;

        public LifetimeController(IFirstCountService firstCount, ISecondCountService secondCount)
        {
            this.firstCount = firstCount;
            this.secondCount = secondCount;
        }

        public IActionResult Test()
        {
            firstCount.GetCounter();
            var result = secondCount.GetCounter();
            return Ok(result);
        }
    }
}
