﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FurrariDemo.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ConfigController : ControllerBase
    {
        private readonly IConfiguration configuration;

        public ConfigController(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        ///config/hello
        //[HttpGet("hello")]
        public IActionResult Hello()
        {
            var result = configuration.GetSection("Furrari");
            return Ok(result.Value);
        }

        [HttpPost("customRoute")]
        public IActionResult Hi()
        {
            return Ok("Ok men");
        }
    }
}
