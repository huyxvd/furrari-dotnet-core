﻿using FurrariDemo.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FurrariDemo.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class FurrariController : ControllerBase
    {
        private readonly IFurrari _furrari;
        public FurrariController(IFurrari furrari)
        {
            _furrari = furrari;
        }

        public IActionResult Hello()
        {
            return Ok(_furrari.GoodJob());
        }

    }
}
